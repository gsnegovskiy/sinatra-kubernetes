require 'rubygems'
require 'sinatra/base'
require 'erb'
require 'fileutils'

SECRET_HASH="a8628380accbd1c10d54d98f3db75601"
TEMPLATE="/var/templates/kube_app_deploy.erb"
DEPLOY="/var/configs/zerodowntime.sh"

class CiUtility < Sinatra::Base

  configure do
    enable :static
  end


  #post request to trigger deploy
  post '/:secret_hash/' do
    if params['secret_hash'] != SECRET_HASH
        status 403
        body "Oops, something gone wrong. Please come back later"
    else
        # needs improvement to be able to locate "tag" in any JSON structure i.e. not only
        # Docker hub webhook.
        payload = JSON.parse(request.body.read)
        status 200
        tag_info = payload["push_data"]["tag"]
        body "Running a deployment for Tag: #{tag_info}"
        run_build tag_info
    end

  end
  #creating a executable file with new "build_tag" which will launch deploy
  def run_build(build_tag)
    erb = ERB.new(File.read(TEMPLATE), 0, '-')
    File.open(DEPLOY, 'w+') do |f|
      f.write(erb.result(binding))
    end
    File.chmod(0700, "#{DEPLOY}")
    `#{DEPLOY}`
  end

end
