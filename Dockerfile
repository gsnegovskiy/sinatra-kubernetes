from alpine

RUN apk add --no-cache ruby curl
ENV INSTALL_DEPS="make gcc ruby-dev libc-dev" \
APP_PATH="/var/opt/sinatra/" \
HTTP_PORT="8088" \
LISTEN_IP="0.0.0.0"

RUN apk update && apk add $INSTALL_DEPS && gem install sinatra \
 json rake --no-document && apk del $INSTALL_DEPS --purge

RUN mkdir -p $APP_PATH /var/configs/

COPY app.rb config.ru $APP_PATH
COPY kube_app_deploy.erb /var/templates/

CMD rackup -o $LISTEN_IP -p $HTTP_PORT $APP_PATH/config.ru
